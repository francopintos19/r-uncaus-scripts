<?php

echo "[".date("Y-m-d H:i:s")."] - \n";
echo "[".date("Y-m-d H:i:s")."] - ---------- INICIO r-uncaus-files.php ---------- \n";

foreach ($files_proyect as $n)
{
	echo "[".date("Y-m-d H:i:s")."] - \n";
	echo "[".date("Y-m-d H:i:s")."] - \n";
	echo "[".date("Y-m-d H:i:s")."] - ..:: ".$n." ::.. \n";
	echo "[".date("Y-m-d H:i:s")."] - \n";

	$proyecto=$files_folder.$n;

	#exec("ssh $us_runcaus@$ip_runcaus 'mkdir -p /home/$us_runcaus/r-uncaus/$host/$n/files'", $output);

	echo "[".date("Y-m-d H:i:s")."] - Copiando archivos a r-uncaus \n";
	$r_output=array();
	#exec("rsync -avh --delete $proyecto $us_runcaus@$ip_runcaus:/home/$us_runcaus/r-uncaus/$host/$n/files", $r_output);
	exec("rsync -avbh --delete --backup-dir=/home/$us_runcaus/r-uncaus/$host/$n/files/backups/backup_$(date +%Y-%m-%d_%H%M) $proyecto $us_runcaus@$ip_runcaus:/home/$us_runcaus/r-uncaus/$host/$n/files", $r_output);
	foreach ($r_output as $r)
	{
		# muestra la salida del rsync
		echo "[".date("Y-m-d H:i:s")."] - ".$r." \n";
	}

	if($r_uncaus_others)
	{
		if($n == $files_proyect_others)
		{
			echo "[".date("Y-m-d H:i:s")."] - Copiando otros archivos a r-uncaus \n";
			#exec("ssh $us_runcaus@$ip_runcaus 'mkdir -p /home/$us_runcaus/r-uncaus/$host/$n/files/others'", $output);
			exec("rsync -avbh --delete --backup-dir=/home/$us_runcaus/r-uncaus/$host/$n/files/others/backups/backup_$(date +%Y-%m-%d_%H%M) $file_others $us_runcaus@$ip_runcaus:/home/$us_runcaus/r-uncaus/$host/$n/files/others", $ro_output);
			foreach ($ro_output as $ro)
			{
				# muestra la salida del rsync
				echo "[".date("Y-m-d H:i:s")."] - ".$ro." \n";
			}
		}
	}
}

echo "[".date("Y-m-d H:i:s")."] - ---------- FIN r-uncaus-files.php ---------- \n";
