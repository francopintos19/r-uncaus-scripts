<?php

if(date('H') > 1 AND date('H') < 6)
{
	$delay_db=rand(1, 10800);
}
else
{
	$delay_db=rand(1, 10);
}

echo "El script r-uncaus-db.php se ejecutara en $delay_db seg \n";
sleep($delay_db); 

echo "[".date("Y-m-d H:i:s")."] - \n";
echo "[".date("Y-m-d H:i:s")."] - ----------- INICIO r-uncaus-db.php ---------- \n";

foreach ($schemas as $n)
{
	echo "[".date("Y-m-d H:i:s")."] - \n";
	echo "[".date("Y-m-d H:i:s")."] - \n";
	echo "[".date("Y-m-d H:i:s")."] - ..:: ".$n." ::.. \n";
	echo "[".date("Y-m-d H:i:s")."] - \n";

	// nombre del archivo a generar
	$filename=$n."_".date("Y-m-d_His").".sql.gz";

	// carpeta de destino del archivo
	$dir = $folder."/r-uncaus-scripts/r-uncaus-db/".$filename;

	// testeamos la conexion antes de empezar
	$conexion = mysqli_connect($host_db, $user_db, $pass_db, $n);
	if (!$conexion)
	{
	    echo "[".date("Y-m-d H:i:s")."] - Error: No se pudo conectar a MySQL." . PHP_EOL;
	    echo "[".date("Y-m-d H:i:s")."] - error de depuración: " . mysqli_connect_errno() . PHP_EOL;
	    echo "[".date("Y-m-d H:i:s")."] - error de depuración: " . mysqli_connect_error() . PHP_EOL;
	    exit;
	}
	echo "[".date("Y-m-d H:i:s")."] - Éxito: Se realizó una conexión apropiada a MySQL!" . PHP_EOL;
	echo "[".date("Y-m-d H:i:s")."] - Información del host: " . mysqli_get_host_info($conexion) . PHP_EOL;
	mysqli_close($conexion);

	echo "[".date("Y-m-d H:i:s")."] - Inicio mysqldump: ".$filename."\n";
	exec("mysqldump -u $user_db -h $host_db --password=$pass_db --default-character-set=utf8 -B $n | gzip > $dir", $output);

	// se crea la carpeta si no existe en el R-UNCAUS
	echo "[".date("Y-m-d H:i:s")."] - Creando la carpeta en r-uncaus\n";
	exec("ssh $us_runcaus@$ip_runcaus 'mkdir -p /home/$us_runcaus/r-uncaus/$host/$n/db'", $output);

	// enviando por ssh al r-uncaus
	echo "[".date("Y-m-d H:i:s")."] - Copiando el archivo: ".$filename."\n";
	exec("rsync -azhpog $dir $us_runcaus@$ip_runcaus:/home/$us_runcaus/r-uncaus/$host/$n/db", $output);

	// borro el archivo enviado
	echo "[".date("Y-m-d H:i:s")."] - Borrando el archivo: ".$filename."\n";
	exec("rm -f $dir", $output);
}

echo "[".date("Y-m-d H:i:s")."] - ---------- FIN r-uncaus-db.php ---------- \n";
